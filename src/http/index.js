import axios from 'axios'
import {Message} from 'element-ui'
import store from '../store/index'
import router from '../router'
const baseURL = 'http://pc.raz-kid.cn:8889/api/private/v1/'
axios.defaults.baseURL = baseURL

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if(response.data.meta.msg === '无效的token') {
        router.push({
            name: 'login'
        })
    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});


export const http =  (url, method='get', data={}, params) => {
    let headers = {}
    return new Promise((resolve, reject)=>{
        axios({
           url,
            headers: {
                authorization: store.state.token,
                'content-type': 'application/json'
            },
           method: method.toLowerCase(),
           data,
           params  
        }).then(res => {
            if (res.status >= 200 && res.status < 300 || res.status===304) {
                if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
                    resolve(res.data)
                }else {
                    Message.error(res.data.meta.msg);
                    reject(res)
                }
            }else {
                Message.error(res.statusText);
                reject(res)
            }
        }).catch(err => {
            reject(err)
        })
    })
}

export const _baseUrl = baseURL