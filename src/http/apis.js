import { http } from './index'
import { Message } from 'element-ui'
//获取菜单权限列表
export function getMenus() {
    return http('menus', 'GET').then(res => {
        return res.data
    })
}
//获取用户列表
export function getUsers(params) {
    return http('users', 'GET',{}, params).then(res => {
        return res.data
    })
}
//添加用户
export function addUsers(data) {
    return http('users', 'POST', data).then(res => {
        Message({
            type: 'success',
            message: '添加成功'
        })
        return res.data
    })
}
//更新用户状态
export function updateUserState(uId, type) {
    return http(`users/${uId}/state/${type}`, 'PUT').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//编辑用户提交
export function editUser(obj) {
    return http(`users/${obj.id}`, 'PUT', {
        email: obj.email,
        mobile: obj.mobile
    }).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//删除用户
export function deleteUser(id) {
    return http(`users/${id}`, 'DELETE').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
    
}

//根据用户id查询用户信息
export function getUsersInfo(id) {
    return http(`users/${id}`).then(res => {
        return res.data
    })
}

//获取角色列表
export function getRolesLists() {
    return http('roles').then(res => {
        return res.data
    })
}

//分配用户角色
export function distributeRole(id, rid) {
    return http(`users/${id}/role`, 'PUT', {
        id,
        rid
    }).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//添加角色
export function addRoles({roleName, roleDesc }) {
    return http('roles', 'POST', {
        roleName,
        roleDesc
    })
    .then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//编辑角色提交
export function editRole({id,roleName,roleDesc}) {
    return http(`roles/${id}`, 'PUT', {
        roleDesc,
        roleName
    })
    .then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//删除权限
export function deleteRights(roleId, rightId) {
    return http(`roles/${roleId}/rights/${rightId}`, 'DELETE').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//获取商品列表

export function getGoodsLists(params) {
    return http('goods', 'GET', {}, params).then(res => {
        return res.data
    })
}

//获取商品分类列表

export function getGoodsCategories() {
    return http('categories', 'GET', {}, {
        type: 3
    }).then(res => {
        return res.data
    })
}

//获取商品参数

export function getGoodsAttrs(id, sel) {
    return http(`categories/${id}/attributes`, 'GET', {}, {
        sel
    }).then(res => {
        return res.data
    })
}

//添加商品

export function addGoods(data) {
    return http('goods', 'POST', data).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}

//根据id查询商品

export function getGoodsDetails(id) {
    return http(`goods/${id}`, 'GET').then(res => {
        return res.data
    })
}

//编辑商品提交
export function editGoods(data) {
    return http(`goods/${data.goods_id}`, 'PUT', data).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}