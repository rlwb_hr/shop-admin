import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function getInitState() {
  return {
    token: localStorage.getItem('token') || '',
    username: localStorage.getItem('username') || ''
  }
}
export default new Vuex.Store({
  state: getInitState(),
  mutations: {
    setToken(state,data) {
      state.token = data
      localStorage.setItem('token', data)
    },
    setUsername(state, data) {
      state.username = data
      localStorage.setItem('username', data)
    },
    initState(state) {
      localStorage.clear()
      Object.assign(state, getInitState())
    }
  },
  actions: {
  },
  modules: {
  }
})
