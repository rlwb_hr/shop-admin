import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import store from '../store'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      notAuthorization: true
    }
  }, {
    path: '/index',
    name: 'index',
    redirect: '/index/users',
    component: () => import(/* webpackChunkName: "index" */ '../views/Index.vue'),
    children: [{
      path: 'users',
      name: 'users',
      component: () => import(/* webpackChunkName: "users" */ '../views/Users.vue'),
    },
      {
        path: 'roles',
        name: 'roles',
        component: () => import(/* webpackChunkName: "roles" */ '../views/rights/Roles.vue'),
      },{
        path: 'goods',
        name: 'goods',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/Goods.vue'),
      }, {
        path: 'goodsadd',
        name: 'goodsadd',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/GoodsAdd.vue'),
      }, {
        path: 'goodsedit/:id',
        name: 'goodsedit',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/GoodsAdd.vue'),
        props: true
      }
    ]
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next)=>{
  if(!to.meta.notAuthorization){
    if(store.state.token){
      next()
    }else {
      next({
        path: '/login'
      })
    }
  }else {
    next()
  }
})

export default router
