# shop-admin

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

###把项目放到远程服务器上

首先，安装xshell,putty客户端，和服务器建立通信使用的工具

其次的话，安装filezilla(ftp上传工具)，服务器上传下载文件使用


把项目上传到nginx的项目目录下边（不同linux操作系统，nginx的项目目录不一样）



## nodejs后台项目api安装，配置，启动

ajax请求传参，请求头设置，表单提交：contentcontent-type: application/x-www-form-urlencoded，
文件上传：content-type: multipart/form-data，
默认格式：content-type: application/json